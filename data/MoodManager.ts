import { onSameDay } from './Date';
import { MoodDate } from './MoodData';
import { MoodDbAdapter } from './MoodDbAdapter';

export class MoodManager {

	private history: MoodDate[];

	private adapter: MoodDbAdapter;

	private get today(): MoodDate {
		return this.history[this.history.length - 1];
	}

	// A promise representing an ongoing current date retrieval operation.
  private currentDatePromise: Promise<MoodDate> = null;

	public moods = [
		"Happy",
		"Sad",
		"Okay",
		"Anxious",
		"Guilty",
		"Energetic",
		"Tired"
	];

	constructor() {
		this.history = [];
		this.adapter = new MoodDbAdapter();
	}

	private async getCurrentDate(): Promise<MoodDate> {
    // Only one current date retrieval operation can occur at once, because multiple of these processes running simultaneously can result in multiple dates being created.
	  // Begin a new current date retrieval operation if none exists.
    if (this.currentDatePromise == null) {
      this.currentDatePromise = this.fetchCurrentDate();
    }
    // Await and return the ongoing currentDatePromise
    return await this.currentDatePromise;
  }

	private async fetchCurrentDate(): Promise<MoodDate> {
	  let recentDate = await this.adapter.getLatestMoodDate();
	  console.warn(recentDate);
		console.log("recentDate:");
		console.log(recentDate);
		if (recentDate == null || !onSameDay(recentDate.date, new Date())) {
      console.log("Creating new date");
      let newDate = new MoodDate();
      await this.adapter.addMoodDate(newDate);
      return newDate;
    } else {
      return recentDate;
    }
	}

	// These methods alter moods for today.

	public async toggleMood(mood: string): Promise<void> {
	  try {
      let today = await this.getCurrentDate();
      today.toggleMood(mood);
      await this.adapter.updateMoodDate(today);
    } catch (error) {
	    console.warn("Update conflict");
    }
    return;
	}

	public async moodsToday(): Promise<string[]> {
		let today = await this.getCurrentDate();
		return today.activeMoods();
	}

	public async availableMoods(): Promise<string[]> {
    let today = await this.getCurrentDate();
    let toRemove: string[] = [];
    if (today.hasMood("Happy")) {
      toRemove.push("Sad");
      toRemove.push("Okay");
    } else if (today.hasMood("Sad")) {
      toRemove.push("Happy");
      toRemove.push("Okay");
    } else if (today.hasMood("Okay")) {
      toRemove.push("Happy");
      toRemove.push("Sad");
    }
    if (today.hasMood("Energetic")) {
      toRemove.push("Tired");
    } else if (today.hasMood("Tired")) {
      toRemove.push("Energetic");
    }
    return this.moods.filter(mood => {
      return !toRemove.includes(mood);
    });
  }

	// This method returns moods for past days.

	public async getHistory(): Promise<MoodRecord[]> {
		let history = await this.adapter.getHistory(30);
		let today = new Date();
    return history.map((moodDate) => {
      return new MoodRecord(moodDate);
    });
	}

	public async debugPrintAllDates(): Promise<void> {
	  let allDates = await this.adapter.getAllDates(20);
	  console.warn(allDates);
  }

}

export class MoodRecord {

	public date: Date;

	public moods: string[];

	public constructor(moodDate: MoodDate) {
		this.date = moodDate.date;
		this.moods = Array.from(moodDate.moods.keys());
	}

}

import { Time } from './Time';

// A template for a repeating self-care activity. Individual instance occur each day where days[dayIndex] is bool at the given time in each week.
export class SelfCareActivity {

	public _id: string;

	public _rev: string;

	public name: string;

	public time: Time;

	public days: Boolean[];

	public constructor(name: string, time: Time, days: Boolean[], _id: string = null, _rev: string = null) {
		if (_id == null) {
			this._id = (Math.random() + 1).toString(36).substring(10);
		} else {
			this._id = _id;
		}
		this._rev = _rev;
		this.name = name;
		this.time = time;
		this.days = new Array<Boolean>(7);
		for (let i = 0; i < 7; i++) {
			if (i < days.length && days[i] === true) {
				this.days[i] = true;
			} else {
				this.days[i] = false;
			}
		}
	}

	public static fromJson(json: any): SelfCareActivity {
		return new SelfCareActivity(json.name, Time.fromJson(json.time), json.days, json._id, json._rev);
	}

	public toJSON(): any {
		return {
			_id: this._id,
      _rev: this._rev,
			name: this.name,
			time: this.time.toJSON(),
			days: this.days
		}
	}

}



// A single occurrence of a self care activity.
export class SelfCareInstance {

	public activity: SelfCareActivity;

	public day: number;

	private _completed: boolean;

	public get completed(): boolean {
		return this._completed;
	}

	public set completed(newValue: boolean){
		if (newValue) {
			this._completed = true;
		}
	}

	public get time() {
		return this.activity.time;
	}

	public get name() {
		return this.activity.name;
	}

	public constructor(activity: SelfCareActivity, day: number) {
		this.activity = activity;
		this.day = day;
		this.completed = false;
	}

	public atSameTimeAs(other: SelfCareInstance): boolean {
		return this.day === other.day && this.time.rawMinutes == other.time.rawMinutes
	}

	public onSameDayAs(other: SelfCareInstance): boolean {
		return this.day === other.day;
	}

}

// An ActivityBlock contains all the self care activities that occur at a single time, which is stored. It also notes whether this time is the earliest time with self care activities for that day.
export class SelfCareBlock {

	public day: number;

	public firstInDay: boolean;

	public time: Time;

	public instances: SelfCareInstance[];

	public constructor(day: number, time: Time) {
		this.day = day;
		this.firstInDay = false;
		this.time = time;
		this.instances = [];
	}

}

// Stores the self care activites performed on a date.
export class SelfCareDate {

	public _id: string;

	public date: Date;

	// The ids of the completed activities on the date.

	public completedActivities: string[];

	public _rev: string;

	public constructor(date: Date, completedActivites: string[]= [], _id: string = null,  _rev: string = undefined) {
		if (_id == null) {
			this._id = date.toJSON();
		} else {
			this._id = _id;
		}
		this.date = date;
		this.completedActivities = completedActivites;
		this._rev = _rev
	}

	public complete(instance: SelfCareInstance) {
		if (!this.completedActivities.includes(instance.activity._id)){
			this.completedActivities.push(instance.activity._id);
		}
	}

	public activityCompletedToday(activity: SelfCareActivity) {
		return this.completedActivities.includes(activity._id);
	}

	public static fromJson(json: any): SelfCareDate {
		console.log("Recreating new activities");
		console.log(json.completedActivites);
		return new SelfCareDate(new Date(json.date), json.completedActivities, json._id, json._rev);
	}

	public toJSON(): any {
		return {
			_id: this._id,
			date: this.date,
			completedActivities: this.completedActivities,
			_rev: this._rev
		}
	}

}


export class Time {
	// The number of minutes past 12:00AM
	public rawMinutes: number;

	// The hours in 24 hour time.
	get hours() {
		return Math.round(this.rawMinutes / 60);
	}

	// Gets the number of hours in 24 hour time.
	get hours12() {
	  let hours = this.hours;
	  if (hours < 12) {
	    return this.hours;
    } else {
	    return this.hours - 12;
    }
  }

  // Returns whether this time is pm, for use with hours12
  get pm() {
	  return this.hours >= 12;
  }

	// The minutes past the hour
	get minutes() {
		return this.rawMinutes % 60;
	}

	constructor(rawMinutes: number) {
		this.rawMinutes = rawMinutes;
	}

	// Creates a Time by extracting the time from the given date.
	public static fromDate(date: Date): Time {
		return new Time((date.getHours() * 60) + date.getMinutes());
	}

	public static fromHoursAndMinutes(hours: number, minutes: number): Time {
	  return new Time((hours * 60) + minutes);
  }

	// Creates a Time based on json data.
	public static fromJson(json: any): Time {
		return new Time(json.rawMinutes);
	}

	public static fromTimeString(string: string): Time {
		let components = string.split(":");
		let hours = Number(components[0]);
		let minutes = Number(components[1]);
		let rawMinutes = (hours * 60) + minutes;
		return new Time(rawMinutes);
	}

	public asDate(): Date {
	  return new Date(0, 0, 0, this.hours, this.minutes);
  }

	// Creates a json representation of the date for storage.
	public toJSON() {
		return {
			rawMinutes: this.rawMinutes
		}
	}

	// Provides a user-readable string representing the time.
	public toString(): string {
		let minuteString = this.minutes.toString();
		if (this.minutes < 10) {
			minuteString = `0${minuteString}`;
		}
		let amPmString: string;
		if (this.pm) {
		  amPmString = "PM";
    } else {
		  amPmString = "AM";
    }
		return `${this.hours12}:${minuteString}${amPmString}`;
	}

}

export function dateStringForDate(date: Date): string {
  return `${stringForDay(date.getDay())}, ${stringForMonth(date.getMonth())} ${date.getDate()}`;
}

export function stringForDay(day: number): string {
  if (day == 0) {
    return "Sunday";
  }
  if (day == 1) {
    return "Monday";
  }
  if (day == 2) {
    return "Tuesday";
  }
  if (day == 3) {
    return "Wednesday";
  }
  if (day == 4) {
    return "Thursday";
  }
  if (day == 5) {
    return "Friday";
  }
  if (day == 6) {
    return "Saturday";
  }
  return "Invalid Date";
}

export function stringForMonth(month: number): string {
  if (month == 0) {
    return "January";
  }
  if (month == 1) {
    return "February";
  }
  if (month == 2) {
    return "March";
  }
  if (month == 3) {
    return "April";
  }
  if (month == 4) {
    return "May";
  }
  if (month == 5) {
    return "June";
  }
  if (month == 6) {
    return "July";
  }
  if (month == 7) {
    return "August";
  }
  if (month == 8) {
    return "September";
  }
  if (month == 9) {
    return "October";
  }
  if (month == 10) {
    return "November";
  }
  if (month == 11) {
    return "December";
  }
}

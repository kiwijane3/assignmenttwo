import { MoodDate } from './MoodData';
import PouchDB from 'pouchdb-react-native';

export class MoodDbAdapter {

	public moodDatabase: PouchDB.Database;

	public constructor() {
		this.moodDatabase = new PouchDB("moodHistory");
	}

	public async addMoodDate(moodDate: MoodDate): Promise<void> {
		await this.moodDatabase.put(moodDate.toJSON());
	}

	public async updateMoodDate(moodDate: MoodDate): Promise<void> {
		await this.moodDatabase.put(moodDate.toJSON(), {
		  force: true
    });
	}

	public async deleteMoodDate(moodDate: MoodDate): Promise<void> {
		await this.moodDatabase.destroy(moodDate.toJSON());
	}

	public async getLatestMoodDate(): Promise<MoodDate> {
		let results = await this.moodDatabase.allDocs({ include_docs: true, limit: 1, descending: true});
		if (results.rows.length > 0) {
			return MoodDate.fromJSON(results.rows[0].doc);
		} else {
			return null;
		}
	}

	// Returns previous mood dates up to limit
	public async getHistory(limit: number): Promise<MoodDate[]> {
		let results = await this.moodDatabase.allDocs({include_docs: true, limit: limit, skip: 1, descending: true});
		return results.rows.map((row) => {
			return MoodDate.fromJSON(row.doc);
		})
	}

	public async getAllDates(limit: number): Promise<MoodDate[]> {
	  let results = await this.moodDatabase.allDocs({
      include_docs: true,
      limit: limit
    });
	  return results.rows.map((row) => {
	    return MoodDate.fromJSON(row.doc);
    })
  }

}

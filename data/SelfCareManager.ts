import { SelfCareActivity, SelfCareBlock, SelfCareDate, SelfCareInstance } from './SelfCareData';

import { SelfCareDbAdapter } from './SelfCareDbAdapter';
import { Time } from './Time';
import { onSameDay, fixDay } from './Date';

export class SelfCareManager {

	private adapter: SelfCareDbAdapter;

	// Records activities that have been completed in the past two days
	private history: SelfCareDate[];

	public nameOfLatestSelfCareActivity: string;

	private currentDatePromise: Promise<SelfCareDate> = null;

	public constructor() {
		this.adapter = new SelfCareDbAdapter();
		this.history = [];
	}

	public addSelfCareActivity(selfCareActivity: SelfCareActivity) {
		this.nameOfLatestSelfCareActivity = selfCareActivity.name;
		this.adapter.addActivity(selfCareActivity);
	}

	public async removeSelfCareActivity(selfCareActivity: SelfCareActivity): Promise<void> {
	  await this.adapter.removeActivity(selfCareActivity);
  }

	public async selfCareActivities(): Promise<SelfCareActivity[]> {
	  return await this.adapter.getActivities();
  }

	public async nextSelfCareInstance(): Promise<SelfCareInstance> {
		let instances = await this.allSelfCareInstances();
		if (instances.length > 0) {
			return instances[0];
		} else {
			return null;
		}
	}

	// Gets the individual instances for this SelfCareActivity that occur within a week.
	public selfCareInstances(activity: SelfCareActivity): SelfCareInstance[] {
		let results: SelfCareInstance[] = [];
		for (let i = 0; i < 7; i++) {
			if (activity.days[i]) {
				results.push(new SelfCareInstance(activity, i));
			}
		}
		return results;
	}

	// Gives all the single instances of self care activities occurring in a week.
  public async allSelfCareInstances(): Promise<SelfCareInstance[]> {
      let result: SelfCareInstance[] = [];
    let currentDate = await this.fetchCurrentSelfCareDate();
    let day = this.today();
    for (let activity of await this.adapter.getActivities()) {
      for (let instance of this.selfCareInstances(activity)) {
        // If the activity is today, then update its completion based on today's date.
        if (instance.day == day && currentDate.activityCompletedToday(instance.activity)) {
          instance.completed = true;
        }
        result.push(instance);
      }
    }
		return result;
	}

	public async selfCareBlocks(daysLimit: number): Promise<SelfCareBlock[]> {
		// Get all instances for the next and order them based on how far in the future they are, so blocks are created in that order.
		let instances = (await this.allSelfCareInstances()).sort((a, b) => {
			if (a.day === b.day) {
				return a.time.rawMinutes - b.time.rawMinutes;
			} else {
				return this.daysInFutureFrom(a.day, this.today()) - this.daysInFutureFrom(b.day, this.today());
			}
		});
		let results: SelfCareBlock[] = [];
		// Iterate through the instances and add to blocks, creating new blocks when the time changes.
		for (let i = 0; i<instances.length;i++) {
			let instance = instances[i];
			// If this is at the same time as the previous instance, add to the current block.
			if (i != 0 && instance.atSameTimeAs(instances[i - 1])) {
				results[results.length - 1].instances.push(instance);
			} else {
				// If this is at a new time, create a new block with the time of the instance.
				let newBlock = new SelfCareBlock(instance.day, instance.time);
				// If this instance is on a different day, then this block is the first of the day, because the instances are ordered with the earliest first. Mark this in the block so that the renderer can show the day.
				if (i == 0 || !instance.onSameDayAs(instances[i - 1])) {
					newBlock.firstInDay = true;
				}
				// Add the instance to the block.
				newBlock.instances.push(instance);
				// Add the block to results.
				results.push(newBlock);
			}
		}
		let today = this.today();
		return results.filter((block) => {
		  return this.daysInFutureFrom(block.day, today) < daysLimit;
    });
	}

	private async getCurrentSelfCareDate() {
	  // Only one current date retrieval operation can occur at once, because multiple of these processes running simultaneously can result in multiple dates being created.
	  // If there is not an ongoing date retrieval operation, start one.
	  if (this.currentDatePromise == null) {
	    this.currentDatePromise = this.fetchCurrentSelfCareDate();
    }
	  // Await and return the ongoing date retrieval operation.
    return await this.currentDatePromise;
  }

	public async fetchCurrentSelfCareDate() {
		let result = await this.adapter.getMostRecentSelfCareDate();
		console.log(result);
		if ((result == null) || (!onSameDay(result.date, new Date()))) {
			console.log("Inserting new self care date");
			let newDate = new SelfCareDate(new Date());
			await this.adapter.addSelfCareDate(newDate);
			return newDate;
		} else {
			return result;
		}
	}

	public async completeActivity(instance: SelfCareInstance): Promise<void> {
		let today = await this.getCurrentSelfCareDate();
		today.complete(instance);
		await this.adapter.updateSelfCareDate(today);
	}

	public today(): number {
		// Move the provided sunday-first day to monday-first
		return fixDay(new Date().getDay());
	}

	public daysInFutureFrom(day: number, start: number) {
		let rawResult = day - start;
		if (rawResult < 0) {
			return 7 + rawResult;
		} else {
			return rawResult;
		}
	}

	public async getHistory(): Promise<SelfCareRecord[]> {
	  let activities = await this.selfCareActivities();
	  let dates = await this.adapter.getHistory(30);
	  return dates.map((selfCareDate) => {
	    let completedActivities = selfCareDate.completedActivities.map((id) => {
	      return activities.find((activity) => {
	        return activity._id == id;
        });
      });
	    return new SelfCareRecord(selfCareDate.date, completedActivities);
    });
  }

  public async debugDisplayAllDates(): Promise<void> {
	  let dates = await this.adapter.getAllDates(20);
    console.warn(dates);
  }

}

export class SelfCareRecord {

  public date: Date;

  public completedActivities: SelfCareActivity[]

  public constructor(date: Date, completedActivities: SelfCareActivity[]) {
    this.date = date;
    this.completedActivities = completedActivities;
  }

}

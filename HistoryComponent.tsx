import React, {Component} from "react";
import {Container, Content, Footer, Left, List, ListItem, Separator, Text} from "native-base";
import {moodManager, selfCareManager} from "./Global";
import {onSameDay} from "./data/Date";
import {SectionList} from "react-native";
import {dateStringForDate} from "./data/Time";

export default class HistoryComponent extends Component {

  public state: any = {
    historySections: []
  }

  static navigationOptions = {
    title: "History"
  }

  public constructor(props: any) {
    super(props);
    // Reload the history when this component is navigated to.
    (this.props as any).navigation.addListener('willFocus', () => {
      this.loadHistory();
    })
  }

  public render(): any {
    console.warn(this.state.historySections);
    return (
      <Container>
        <Content style={{backgroundColor: "#F0EFF5"}}>
          { this.showPlaceholder() && <Text>
            No History available
          </Text>}
          <SectionList style={{ flex: 1, backgroundColor: "#FFFFFF" }} sections={this.state.historySections}
                       renderSectionHeader={({section: { date }}) =>
                         (
                           <Separator bordered>
                             <Text>
                               {dateStringForDate(date)}
                             </Text>
                           </Separator>
                         )
                       }
                       renderItem={({ item }) => (<ListItem>
                         <Left>
                           <Text>
                             {item}
                           </Text>
                         </Left>
                       </ListItem>)}
            />
        </Content>
      </Container>
    )
  }

  // Fetches history records, and converts them into a format readable by the section list.
  public async loadHistory() {
    let moodRecords = await moodManager().getHistory();
    let selfCareRecords = await selfCareManager().getHistory();
    let sections: any[] = [];
    // Begin by creating sections for each moodRecord. Each section contains two fields; A date representing the day represented, and a list of strings with history records.
    moodRecords.forEach((record) => {
      sections.push({
        date: record.date,
        data: record.moods
      });
    })
    // Now, add data for self care records, either to an existing section, or to a new section, if necessary. If the activity no longer exists, convert it to null and filter nulls out.
    selfCareRecords.forEach((record) => {
      let activityNames = record.completedActivities.map((activity) => {
        if (activity) {
          return activity.name;
        } else {
          return null;
        }
      }).filter((name) => {
        return name != null;
      });
      // Attempt to find a section on the same day as this self care record.
      let section = sections.find((section) => {
        return onSameDay(record.date, section.date);
      });
      // If a section has been found, Add the names of the self care activities to the section's data array
      if (section != null) {
        activityNames.forEach((name) => {
          section.data.push(name);
        })
      } else {
        // Otherwise, create a new section with the names of the completed activities as data.
        sections.push({
          date: record.date,
          data: activityNames
        });
      }
    });
    // If sections are created from self care records, they will be out of order since that occurs after all mood records are processed.
    // As such, it is necessary to sort the sections explicitly by date, with the latest dates first.
    sections = sections.sort((a, b) => {
      if (a.date < b.date) {
        return 1;
      } else if (a.date > b.date) {
        return -1
      } else {
        return 0;
      }
    });
    console.warn(sections);
    // Remove empty sections.
    sections = sections.filter((section) => {
      return section.data.length > 0;
    })
    this.setState({
      historySections: sections
    });
  }

  // Indicates whether a placeholder label should be displayed.
  public showPlaceholder() {
    return this.state.historySections.length < 1;
  }

  public async debugPrintDatabase(): Promise<void> {
    moodManager().debugPrintAllDates();
    selfCareManager().debugDisplayAllDates();
  }

}

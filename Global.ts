import {SelfCareManager} from "./data/SelfCareManager";
import EventEmitter from "wolfy87-eventemitter";
import {MoodManager} from "./data/MoodManager";

export interface Global {

  eventEmitter: EventEmitter;

  selfCareManager: SelfCareManager;

}

export function eventEmitter(): EventEmitter {
  return (global as any).eventEmitter;
}

export function selfCareManager(): SelfCareManager {
  return (global as any).selfCareManager;
}

export function moodManager(): MoodManager {
  return (global as any).moodManager;
}

// Initialises global variables.
export function initGlobal() {
  (global as any).eventEmitter = new EventEmitter();
  (global as any).selfCareManager = new SelfCareManager();
  (global as any).moodManager = new MoodManager();
}

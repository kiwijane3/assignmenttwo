import {Component} from "react";
import {View} from "react-native";
import {Button, Container, Content, Text} from "native-base";
import React from "react";

export default class BuddyComponent extends Component {

  static navigationOptions = ({ navigation }) => ({
    title: "Buddy",
    headerRight: (<Button transparent onPress={ () => navigation.navigate("Record")}>
      <Text>
        Record
      </Text>
    </Button>)
  })

  render(): any {
    return (
      <Container>
        <Content>
          <Text>
            Buddy Component Placeholder
          </Text>
        </Content>
      </Container>
    );
  }

}

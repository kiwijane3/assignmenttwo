import {Component} from "react";
import {
  Body,
  Button,
  Container,
  Content,
  Icon,
  Left,
  ListItem,
  Right,
  Text,
} from "native-base";
import React from "react";
import {SelfCareActivity} from "./data/SelfCareData";
import {selfCareManager} from "./Global";
import {Alert, FlatList} from "react-native";

export default class SelfCareListComponent extends Component {

  static navigationOptions = ({ navigation }) => ({
    title: "Self Care Activities",
    headerRight: (<Button transparent onPress={ () => navigation.navigate("SelfCareCreate") }>
      <Text>
        New
      </Text>
    </Button>)
  });

  public state: any = {
    activities: []
  }

  public constructor(props: any) {
    super(props);
    // Reload the activities when the component is loaded.
    (this.props as any).navigation.addListener('willFocus', () => {
      this.loadActivities();
    });
  }

  public render(): any {
    return (
      <Container>
        <Content style={{ backgroundColor: "#F0EFF5"}}>
          { this.showPlaceholder() && <Text>No Activities</Text>}
          <FlatList style={{ flex: 1, backgroundColor: "#FFFFFF" }} data={ this.state.activities } renderItem={({ item }) => (
            <ListItem>
              <Left>
                <Text>
                  { (item as SelfCareActivity).name }
                </Text>
              </Left>
              <Body style={{ alignItems: "flex-end" }}>
                <Text>
                  { this.daysString((item as SelfCareActivity).days)}
                </Text>
                <Text>
                  { (item as SelfCareActivity).time.toString() }
                </Text>
              </Body>
              <Right>
                <Button transparent onPress={ () => this.showDeleteAlert((item as SelfCareActivity))}>
                  <Icon  name="trash" style={{ color: "red",  fontSize: 20}}></Icon>
                </Button>
              </Right>
            </ListItem>
          )}>
          </FlatList>
        </Content>
      </Container>
    )
  }

  // Fetches the activity templates and updates the state.
  public async loadActivities() {
    let activities = await selfCareManager().selfCareActivities();
    this.setState({
      activities: activities
    })
  }

  // Returns a human-readable string representation of the days an activity occurs on.
  public daysString(days: Boolean[]) {
    let output = "";
    // If the event occurs every day of the week, return "Everyday"
    if (!days.includes(false)) {
      return "Everyday";
    }
    // Otherwise, add a symbol for each day it occurs on, with a space as a separator.
    if (days[0]) {
      output = output + "M ";
    }
    if (days[1]) {
      output = output + "Tu ";
    }
    if (days[2]) {
      output = output + "W ";
    }
    if (days[3]) {
      output = output + "Th ";
    }
    if (days[4]) {
      output = output + "F ";
    }
    if (days[5]) {
      output = output + "Sa ";
    }
    if (days[6]) {
      output = output + "Su ";
    }
    // Remove trailing whitespace to avoid wasted space
    return output.trim();
  }

  // Indicates whether a placeholder label should be displayed
  public showPlaceholder() {
    return this.state.activities.length < 1;
  }

  public showDeleteAlert(activity: SelfCareActivity) {
    Alert.alert(
      `Really delete ${activity.name}`,
      "",
      [
        {
          text: "No"
        },
        {
          text: "Yes",
          onPress: () => this.deleteActivity(activity)
        }
      ]
    )
  }

  public async deleteActivity(activity: SelfCareActivity) {
    await selfCareManager().removeSelfCareActivity(activity);
    this.loadActivities()
  }

}

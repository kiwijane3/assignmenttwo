import {Component} from "react";
import {
  CheckBox,
  Container,
  Content,
  Form,
  Input,
  Item,
  Body, Left,
  List,
  ListItem,
  Picker,
  Right,
  Separator,
  Text, Button, Toast, Icon, Footer
} from "native-base";
import React from "react";
import {Time} from "./data/Time";
import {SelfCareActivity} from "./data/SelfCareData";
import {eventEmitter} from "./Global";


export default class CreateSelfCareActivityComponent extends Component {

  static navigationOptions = {
    title: "Create Activity",
    headerRight: (
      <Button transparent onPress={() => eventEmitter().emit("createActivity")}>
        <Text>
          Create
        </Text>
      </Button>
    )
  }

  public state: any;

  public constructor(props: any) {
    super(props);
    this.state = {
      name: "",
      time: Time.fromHoursAndMinutes(25, 0),
      show: false,
      days: [false, false, false, false, false, false, false]
    };
    // Subscribe to events from the creation button in the navigation bar in order to create the described component.
    (global as any).eventEmitter.addListener("createActivity", () => this.create())
  }

  render(): any {
    return (
      <Container>
        <Content style={{ backgroundColor: "#F0EFF5"}}>
          <Form style={{ backgroundColor: "#FFFFFF" }}>
            <Item inlineLabel>
              <Left>
                <Text>
                  Activity Name
                </Text>
              </Left>
                <Input style={{ textAlign: "right"}}value={this.state.name} onChangeText={ (text) => this.setName(text)}/>
            </Item>
            <Item picker inlineLabel>
              <Left>
                <Text>
                  Time
                </Text>
              </Left>
              <Right>
                <Picker
                  mode="dropdown"
                  iosIcon={<Icon name="arrow-down"/>}
                  placeholder="Select Activity Time"
                  selectedValue={this.state.time.rawMinutes}
                  onValueChange={(value) => {
                    this.setTime(value);
                  }}>
                  <Picker.Item label="6AM" value={Time.fromHoursAndMinutes(6, 0).rawMinutes}/>
                  <Picker.Item label="7AM" value={Time.fromHoursAndMinutes(7, 0).rawMinutes}/>
                  <Picker.Item label="8AM" value={Time.fromHoursAndMinutes(8, 0).rawMinutes}/>
                  <Picker.Item label="9AM" value={Time.fromHoursAndMinutes(9, 0).rawMinutes}/>
                  <Picker.Item label="10AM" value={Time.fromHoursAndMinutes(10, 0).rawMinutes}/>
                  <Picker.Item label="11AM" value={Time.fromHoursAndMinutes(11, 0).rawMinutes}/>
                  <Picker.Item label="12PM" value={Time.fromHoursAndMinutes(12, 0).rawMinutes}/>
                  <Picker.Item label="1PM" value={Time.fromHoursAndMinutes(13, 0).rawMinutes}/>
                  <Picker.Item label="2PM" value={Time.fromHoursAndMinutes(14, 0).rawMinutes}/>
                  <Picker.Item label="3PM" value={Time.fromHoursAndMinutes(15, 0).rawMinutes}/>
                  <Picker.Item label="4PM" value={Time.fromHoursAndMinutes(16, 0).rawMinutes}/>
                  <Picker.Item label="5PM" value={Time.fromHoursAndMinutes(17, 0).rawMinutes}/>
                  <Picker.Item label="6PM" value={Time.fromHoursAndMinutes(18, 0).rawMinutes}/>
                  <Picker.Item label="7PM" value={Time.fromHoursAndMinutes(19, 0).rawMinutes}/>
                  <Picker.Item label="8PM" value={Time.fromHoursAndMinutes(20, 0).rawMinutes}/>
                  <Picker.Item label="9PM" value={Time.fromHoursAndMinutes(21, 0).rawMinutes}/>
                </Picker>
              </Right>
            </Item>
             <Separator bordered>
               <Text>
                 Days
               </Text>
             </Separator>
             <ListItem onPress={() => this.toggleDay(0)}>
               <Left>
                <Text>Monday</Text>
               </Left>
               <Right>
                 <CheckBox checked={this.onDay(0)} onPress={() => this.toggleDay(0)}/>
               </Right>
             </ListItem>
             <ListItem onPress={() => this.toggleDay(1)}>
               <Left>
                 <Text>Tuesday</Text>
               </Left>
               <Right>
                 <CheckBox checked={this.onDay(1)} onPress={() => this.toggleDay(1)}/>
               </Right>
             </ListItem>
             <ListItem onPress={() => this.toggleDay(2)}>
               <Left>
                 <Text>Wednesday</Text>
               </Left>
               <Right>
                 <CheckBox checked={this.onDay(2)} onPress={() => this.toggleDay(2)}/>
               </Right>
             </ListItem>
             <ListItem onPress={() => this.toggleDay(3)}>
               <Left>
                 <Text>Thursday</Text>
               </Left>
               <Right>
                 <CheckBox checked={this.onDay(3)} onPress={() => this.toggleDay(3)}/>
               </Right>
             </ListItem>
             <ListItem onPress={() => this.toggleDay(4)}>
               <Left>
                 <Text>Friday</Text>
               </Left>
               <Right>
                 <CheckBox checked={this.onDay(4)} onPress={() => this.toggleDay(4)}/>
               </Right>
             </ListItem>
             <ListItem onPress={() => this.toggleDay(5)}>
               <Left>
                 <Text>Saturday</Text>
              </Left>
             <Right>
               <CheckBox checked={this.onDay(5)} onPress={() => this.toggleDay(5)}/>
             </Right>
            </ListItem>
             <ListItem onPress={() => this.toggleDay(6)} >
             <Left>
               <Text>Sunday</Text>
             </Left>
             <Right>
               <CheckBox checked={this.onDay(6)} onPress={() => this.toggleDay(6)}/>
             </Right>
           </ListItem>
            <Footer/>
          </Form>
        </Content>
      </Container>
    )
  }

  public setName(value: string) {
    this.setState({
      name: value
    })
  }

  // Sets the time based on a raw json object, as the selector item need to be in this format for the selector to recognise they have been selected.
  public setTime(value: any) {
    this.setState({
      time: new Time(value)
    })
  }
  public toggleDay(dayIndex) {
    let days = this.state.days;
    days[dayIndex] = !days[dayIndex];
    this.setState({
      days: days
    })
  }

  // Returns whether the event will occur on the day with the respective index. Indexes start at 0 for monday and 6 for saturday.
  public onDay(dayINdex): boolean {
    return this.state.days[dayIndex];
  }

  // Sanity checks the activity
  public validate(): boolean {
    // 25 hours is basically a magic number for an unedited time.
    return this.state.name.trim() != "" && this.state.time.hours != 25 && this.onAtLeastOneDay();
  }

  public onAtLeastOneDay() {
    return this.state.days.includes(true);
  }

  public create() {
    // Check the activity is valid before creating it.
    if (this.validate()) {
      (global as any).selfCareManager.addSelfCareActivity(new SelfCareActivity(this.state.name, this.state.time, this.state.days));
      (this.props as any).navigation.goBack();
    }
  }

}
/**
 **/

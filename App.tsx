/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import SelfCareListComponent from "./SelfCareList";
import {createStackNavigator} from "react-navigation-stack";
// @ts-ignore
import {createBottomTabNavigator} from "react-navigation-tabs";
import BuddyComponent from "./BuddyComponent";
import HistoryComponent from "./HistoryComponent";
import {createAppContainer} from "react-navigation";
import CreateSelfCareActivityComponent from "./CreateSelfCareActivity";
import RecordComponent from "./RecordComponent";
import EventEmitter from "wolfy87-eventemitter";
import {SelfCareManager} from "./data/SelfCareManager";
import {initGlobal} from "./Global";
import TodayComponent from "./TodayComponent";
import Icon from "react-native-vector-icons/FontAwesome";


initGlobal();

// Create the navigation component for the self care tab
const selfCareStack = createStackNavigator({
    SelfCareList: SelfCareListComponent,
    SelfCareCreate: CreateSelfCareActivityComponent
  }
);

const todayStack = createStackNavigator({
  Today: TodayComponent
});

const historyComponent = createStackNavigator({
  History: HistoryComponent
})

const tabs = createBottomTabNavigator({
  Today: todayStack,
  SelfCare: selfCareStack,
  History: historyComponent
}, {
  defaultNavigationOptions: ({navigation}) => ({
    tabBarIcon: ({ focused, horizontal, tintColor}) => {
      let iconString = "question-circle";
      let routeName = navigation.state.routeName;
      if (routeName == "Today") {
        iconString = "calendar";
      } else if (routeName == "History") {
        iconString = "history";
      } else if (routeName == "SelfCare") {
        iconString = "list";
      }
      return <Icon name={iconString} size={20} color={tintColor}/>
    }
  })

})

export default createAppContainer(tabs);

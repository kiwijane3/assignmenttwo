import React, {Component} from "react";
import {Button, CheckBox, Container, Content, Footer, Label, Left, ListItem, Right, Separator, Text} from "native-base";
import {Image, SectionList, View} from "react-native";
import MoodChip from "./MoodChip";
import {SelfCareActivity, SelfCareBlock, SelfCareInstance} from "./data/SelfCareData";
import {eventEmitter, moodManager, selfCareManager} from "./Global";

export default class TodayComponent extends Component {

  static navigationOptions = {
    title: "Today"
  }

  public state = {
    selfCareBlocks: [],
    buddyAsset: require("./assets/RaccoonNeutral.png"),
    buddyMessage: "Hi! I hope you're doing well!"
  }

  // The route to the image for the buddy.
  public get buddyAsset(): any {
    return (this.state as any).buddyAsset;
  }

  public get buddyMessage(): string {
    return (this.state as any).buddyMessage;
  }

  public get selfCareBlocks(): SelfCareBlock[] {
    return (this.state as any).selfCareBlocks
  }

  // Converts self care blocks into a format the compatible with the section list interface
  public get selfCareSections(): any {
    return this.selfCareBlocks.map((block) => {
      return {
        block: block,
        data: block.instances
      }
    })
  }

  public constructor(props) {
    super(props);
    // Update the mood buddy in response to mood changes.
    eventEmitter().addListener("MoodUpdated", () => {
      this.loadBuddy();
    });
    // Reload the state when this page is navigated to.
    (this.props as any).navigation.addListener('willFocus', () => {
      this.loadBuddy();
      this.loadBlocks();
    })
  }

  public render(): any {
    return (
      <Container>
        <Content style={{ backgroundColor: "#F0EFF5" }}>
          <View style={{ backgroundColor: "#FFFFFF"}}>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <Image source={this.buddyAsset} style={{ flex: 1, aspectRatio: 1, margin: 8 }}/>
              <Label style={{ flex: 2, margin: 8}}>{this.buddyMessage}</Label>
            </View>
            <Separator>
              <Text>
                Moods
              </Text>
            </Separator>
            <View style={{ flexDirection: "row", width: "100%", flexWrap: "wrap" }}>
              <MoodChip mood="Happy"/>
              <MoodChip mood="Sad"/>
              <MoodChip mood="Okay"/>
              <MoodChip mood="Anxious"/>
              <MoodChip mood="Guilty"/>
              <MoodChip mood="Energetic"/>
              <MoodChip mood="Tired"/>
            </View>
            <SectionList sections={this.selfCareSections} renderSectionHeader={({section: {block}}) => {
              return (
                <Separator bordered>
                  <Text>
                    {block.time.toString()}
                  </Text>
                </Separator>
              )
            }} renderItem={({item}) => {
              return (
                <ListItem onPress={() => this.completeActivity(item)}>
                  <Left>
                    <Text>
                      {item.activity.name}
                    </Text>
                  </Left>
                  <Right>
                    <CheckBox checked={item.completed} onPress={() => this.completeActivity(item)}/>
                  </Right>
                </ListItem>
              )
            }}/>
          </View>
        </Content>
      </Container>
    );
  }

  // Loads self-care activities.
  public async loadBlocks() {
    let blocks = await selfCareManager().selfCareBlocks(1);
    this.setState({
      selfCareBlocks: blocks
    })
  }

  // Updates mood buddy image and message based on
  public async loadBuddy() {
    let moods = await moodManager().moodsToday();
    if (moods.includes("Happy")) {
      this.setState({
        buddyAsset: require("./assets/RaccoonCelebrating1.png"),
        buddyMessage: "Hi! I'm glad you're doing well!"
      });
    } else if (moods.includes("Okay")) {
      this.setState({
        buddyAsset: require("./assets/RaccoonCuddleHappy.png"),
        buddyMessage: "Hi! I'm glad you're doing okay. Hope you have a good day."
      });
    } else if (moods.includes("Sad")) {
      this.setState({
        buddyAsset: require("./assets/RaccoonCuddleSad.png"),
        buddyMessage: "Hi! I'm sorry you're feeling sad. I hope you feel better soon"
      })
    } else {
      this.setState({
        buddyAsset: require("./assets/RaccoonNeutral.png"),
        buddyMessage: "Hi! I hope you're doing well!"
      })
    }
  }

  public async completeActivity(instance: SelfCareInstance) {
    await selfCareManager().completeActivity(instance);
    this.loadBlocks();
  }

}

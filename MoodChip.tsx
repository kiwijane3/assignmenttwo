import {Component} from "react";
import {eventEmitter, moodManager} from "./Global";
import {Button, Text} from "native-base";
import {View} from "react-native";
import React from "react";

export default class MoodChip extends Component {

  public state = {
    display: false,
    active: false
  }

  public get mood(): string {
    return (this.props as any).mood;
  }

  public get display(): boolean {
    return (this.state as any).display;
  }

  // Whether the mood represented by this chip is selected for the current day.
  public get active(): boolean {
    return (this.state as any).active;
  }

  public constructor(props: any) {
    super(props);
    // Update this component in response to updates to the mood state.
    eventEmitter().addListener("MoodUpdated", () => this.loadDisplay());
    this.loadActive();
    this.loadDisplay();
  }

  public render(): any {
    return (<View>
      { this.display &&
        <Button onPress={() => this.toggle()} light={!this.active} primary={this.active} style={{ margin: 8 }}>
          <Text>
            {this.mood}
          </Text>
        </Button>
      }
      </View>
    )
  }

  public async toggle() {
    await moodManager().toggleMood(this.mood);
    this.loadActive();
    // Emit an event to notify other mood chips to reload.
    eventEmitter().emit("MoodUpdated");
  }

  public async loadDisplay() {
    let availableMoods = await moodManager().availableMoods();
    this.setState({
      display: availableMoods.includes(this.mood)
    });
  }

  // Fetches information about whether this event is currently selected for the current day.
  public async loadActive() {
    let moodsToday = await moodManager().moodsToday();
    this.setState({
      active: moodsToday.includes(this.mood)
    });
  }

}


